*NOTE: Api keys are removed from `src/config.json` to prevent potential abuse (since they are not bound to a specific domain).*

# Requirements - globally installed
* [Node.js](https://nodejs.org/en/).
* [npm](https://www.npmjs.com/) (included in Node.js).
* [gulp](https://gulpjs.com/docs/en/getting-started/quick-start).

# How-to
* Install packages: `npm i`.
* Start local development environment: `gulp dev`.
* For more information, see [frontend-blueprint](https://gitlab.com/bramswier/frontend-blueprint).

# About
This is a small web app written in vanilla JavaScript with modern techniques to display the actual weather of cities in The Netherlands. A city name or the user's location can be searched on. The language of the web app is Dutch.

It uses the weather API from http://weerlive.nl/delen.php. The Maps API and Geocode API are used from https://tech.yandex.com/maps/.

It is built upon the [frontend-blueprint](https://gitlab.com/bramswier/frontend-blueprint).

## Overview of techniques used
### HTML
- Semantic elements and WAI-ARIA.
- Data-attributes instead of classes to be targeted by JavaScript.
- Written in nunjucks.

### CSS
- Grid and Flexbox.
- rem and em.
- BEM naming convention.
- Written in SCSS.
- Autoprefixing based on supported browsers defined in the browserslist.

### JavaScript
- Mustard cut and polyfilling.
- ES2015+.
    - Classes as modules.
    - Fetch and Promise chaining.
    - for...of with Array destructuring.
    - Map and an observer as singletons.
	- Browserify and Babel to bundle and transpile. Transpiling depends on supported browsers defined in the browserslist.
	- Naming convention for private properties and methods.
- Geolocation.
- JSDoc.

### Images
- Automatically generated SVG icon sprite.

### Fonts
- Font loading strategy using CSS, JavaScript and LocalStorage to display a *flash of unstyled text* instead of a *flash of invisible text* while the fonts are loading.

### Quality control
- ESLint and Stylelint triggering on git commit.
