import state from '../../static/js/utils/state';
import observer from '../../static/js/utils/observer';

export default class {
	/**
	* @param {HTMLElement} element
	* @param {Object} options
	*/
	constructor(element, options) {
		this._element = element;
		this._options = options;

		this._contentElement = this._element.querySelector('[data-blocks-content]');

		this._placeElement = this._element.querySelector('[data-place]');
		this._iconElement = this._element.querySelector('[data-icon]');
		this._summaryElement = this._element.querySelector('[data-summary]');
		
		this._temperatureElement = this._element.querySelector('[data-temperature]');
		this._feelingTemperatureElement = this._element.querySelector('[data-feeling-temperature]');

		this._precipationRateElement = this._element.querySelector('[data-precipation-rate]');

		this._windSpeed = this._element.querySelector('[data-wind-speed]');
		this._windDirection = this._element.querySelector('[data-wind-direction]');

		this._sunUpElement = this._element.querySelector('[data-sun-up]');
		this._sunUnderElement = this._element.querySelector('[data-sun-under]');

		this._spinnerElement = this._element.querySelector('[data-spinner]');
		this._errorElement = this._element.querySelector('[data-error]');

		// `bind` to maintain context upon call by observer
		observer.subscribe(this._showSpinner.bind(this), 'searching');
		observer.subscribe(this._displayData.bind(this), 'data-stored');
		observer.subscribe(this._showError.bind(this), 'error');
	}

	_displayData() {
		this._placeElement.innerHTML = state.get('plaats');
		this._iconElement.className = `icon-${state.get('image')}`;
		
		this._summaryElement.innerHTML = state.get('samenv');
		
		this._temperatureElement.innerHTML = state.get('temp');
		this._feelingTemperatureElement.innerHTML = state.get('gtemp');

		this._precipationRateElement.innerHTML = state.get('d0neerslag');

		this._windSpeed.innerHTML = state.get('windkmh');
		this._windDirection.innerHTML = state.get('windr');

		this._sunUpElement.innerHTML = state.get('sup');
		this._sunUnderElement.innerHTML = state.get('sunder');

		this._showBlocks();
	}

	_showBlocks() {
		this._contentElement.classList.add('is-visible');
		this._contentElement.setAttribute('aria-hidden', false);
		this._contentElement.setAttribute('role', 'alert');

		this._hideSpinner();
	}

	_showSpinner() {
		this._hideError();
		
		this._element.classList.add('is-visible');
		this._spinnerElement.classList.remove('is-hidden');
	}

	_hideSpinner() {
		this._spinnerElement.classList.add('is-hidden');
	}

	_showError() {
		this._errorElement.classList.remove('is-hidden');
		this._errorElement.setAttribute('aria-hidden', false);
		this._errorElement.setAttribute('role', 'alert');

		this._hideSpinner();
	}

	_hideError() {
		this._errorElement.classList.add('is-hidden');
		this._errorElement.setAttribute('aria-hidden', true);
		this._errorElement.removeAttribute('role');
	}
}
