import state from '../../static/js/utils/state';
import observer from '../../static/js/utils/observer';

export default class {
	/**
	* @param {HTMLElement} element
	* @param {Object} options
	*/
	constructor(element, options) {
		this._element = element;
		this._options = options;

		this._searchInputElement = this._element.querySelector('[data-search-input]');
		this._gpsButtonElement = this._element.querySelector('[data-gps-button]');

		this._addEventListeners();
		this._checkNavigator();
	}

	_addEventListeners() {
		this._element.addEventListener('submit', event => {
			event.preventDefault();
			
			const value = this._searchInputElement.value;
			this._searchLocation(value);
		});

		this._gpsButtonElement.addEventListener('click', () => {
			observer.publish('searching');
			
			navigator.geolocation.getCurrentPosition(position => {
				const coordinates = `${position.coords.latitude},${position.coords.longitude}`;
				this._searchLocation(coordinates);
			}, () => {
				observer.publish('error');
			});
		});
	}

	_checkNavigator() {
		if ('geolocation' in navigator) {
			this._gpsButtonElement.classList.add('is-visible');
			this._gpsButtonElement.setAttribute('aria-hidden', 'false');
			this._gpsButtonElement.setAttribute('role', 'alert');
		}
	}

	/**
	 * @param {String} location
	 */
	_searchLocation(location) {
		observer.publish('searching');

		const apiEndpoint = this._options.apiEndpoint;
		const apiKey = this._options.apiKey;

		fetch(`${apiEndpoint}?key=${apiKey}&locatie=${location}`)
			.then(response => response.json())
			.then(rawData => this._storeData(rawData))
			.catch(() => observer.publish('error'));
	}

	/**
	 * @param {Object} rawData 
	 */
	_storeData(rawData) {
		const data = rawData.liveweer[0];

		for (const [key, value] of Object.entries(data)) {
			state.set(key, value);
		}

		observer.publish('data-stored');
	}
}
