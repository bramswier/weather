/*global ymaps*/

import state from '../../static/js/utils/state';
import observer from '../../static/js/utils/observer';

export default class {
	/**
	* @param {HTMLElement} element
	* @param {Object} options
	*/
	constructor(element, options) {
		this._element = element;
		this._options = options;

		this._contentElement = this._element.querySelector('[data-blocks-content]');

		this._spinnerElement = this._element.querySelector('[data-spinner]');
		this._errorElement = this._element.querySelector('[data-error]');

		this._apiEndpoint = this._options.apiEndpoint;
		this._apiKey = this._options.apiKey;

		this._mapIsRendered = false;
		this._mapInstance = {};

		// `bind` to maintain context upon call by observer
		observer.subscribe(this._showSpinner.bind(this), 'searching');
		observer.subscribe(this._getCoordinates.bind(this), 'data-stored');
		observer.subscribe(this._showError.bind(this), 'error');
	}

	_getCoordinates() {
		const apiEndpoint = this._apiEndpoint;
		const apiKey = this._apiKey;
		const location = state.get('plaats');

		fetch(`${apiEndpoint}?apikey=${apiKey}&geocode=${location},+Netherlands&lang=en_US&format=json`)
			.then(response => response.json())
			.then(rawData => this._renderLocation(rawData))
			.catch(() => this._showError());
	}

	/**
	 * @param {Object} rawData 
	 */
	_renderLocation(rawData) {
		/** @type {String} - lon lat */
		const pos = rawData.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
		const [lon, lat] = pos.split(' ');

		if (this._mapIsRendered) {
			this._setPlaceMark(lat, lon);
			return;
		}

		this._renderMap();
		this._setPlaceMark(lat, lon);
		this._mapIsRendered = true;
	}

	_renderMap() {
		this._mapInstance = new ymaps.Map(this._element, {
			center: [52.090736, 5.121420], // Utrecht
			zoom: 7,
			controls: []
		});

		this._hideSpinner();
		this._element.classList.add('is-visible');
	}

	_setPlaceMark(lat, lon) {
		this._removeAllPlaceMarks();
		
		const placeMark = new ymaps.Placemark([lat, lon]);
		this._mapInstance.geoObjects.add(placeMark);

		this._hideSpinner();
	}

	_removeAllPlaceMarks() {
		this._mapInstance.geoObjects.removeAll();
	}

	_showSpinner() {
		this._hideError();

		if (!this._mapIsRendered) {
			this._element.classList.add('is-visible');
			this._spinnerElement.classList.remove('is-hidden');
		}	
	}

	_hideSpinner() {
		this._spinnerElement.classList.add('is-hidden');
	}

	_showError() {
		this._errorElement.classList.remove('is-hidden');
		this._errorElement.setAttribute('aria-hidden', false);
		this._errorElement.setAttribute('role', 'alert');

		this._hideSpinner();
	}

	_hideError() {
		this._errorElement.classList.add('is-hidden');
		this._errorElement.setAttribute('aria-hidden', true);
		this._errorElement.removeAttribute('role');
	}
}
