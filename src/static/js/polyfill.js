import 'core-js/fn/array/from';
import 'core-js/fn/promise';
import 'core-js/fn/object/entries';
import 'core-js/fn/map';
import 'core-js/fn/symbol';
import 'core-js/fn/dom-collections/iterator';
import 'whatwg-fetch';
