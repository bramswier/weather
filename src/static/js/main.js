// Vendors
import FontFaceObserver from 'fontfaceobserver';

// Modules
import Search from '../../components/search/Search';
import Blocks from '../../components/blocks/Blocks';
import Map from '../../components/map/Map';

const modules = { Search, Blocks, Map };

// Create an instance of modules found on the page (`data-module` attribute on an element), 
// optionally supplying an options object (`data-options` attribute on the same element)
for (const element of document.querySelectorAll('[data-module]')) {
	new modules[element.dataset.module](element, element.dataset.options ? JSON.parse(element.dataset.options) : '');
}

// Fonts were not loaded before (this is the first website visit), load fonts
if (!localStorage.getItem('fonts-loaded')) {
	const fontNotoSans = new FontFaceObserver('Noto Sans');

	fontNotoSans.load().then(() => {
		document.documentElement.classList.add('fonts-loaded');
		localStorage.setItem('fonts-loaded', 'true');
	});
}

