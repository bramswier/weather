class Observer {
	constructor() {
		this._observers = new Map();
	}

	/**
	 * @param {Function} fn
	 * @param {String} message
	 */
	subscribe(fn, message) {
		this._observers.set(fn, message);
	}

	/**
	 * @param {String} message 
	 */
	publish(message) {
		for (const [fn, subscribedMessage] of this._observers.entries()) {
			if (message === subscribedMessage) {
				this._call(fn);
			}
		}
	}

	/**
	 * @param {Function} fn 
	 */
	_call(fn) {
		fn();
	}
}

// singleton
export default new Observer();
